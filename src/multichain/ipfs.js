import axios from 'axios';

const baseUrl1 = 'http://34.207.213.121:3500';


export async function storeIpfs(GTNID, actorRole, receivedIpfsHash) {
   var result = {
       txnHash: '',
       eventDate: '',
       errorMsg: '',
   };
   try {
       result.eventDate = new Date().toLocaleString();
       var dataArgs = {
           stream: 'ipfs_stream',
           key: GTNID, 
           data: {
               date: result.eventDate,
               actor: actorRole,  
               eventName: 'storeIpfs',
	       ipfsHash1: receivedIpfsHash,
           },
       };
       var res = await axios.post(baseUrl1 + '/publish', dataArgs);
       console.log('Hash:' + res.data.transactionId);        
       result.txnHash = res.data.transactionId;
       result.errorMsg = res.error;
   } catch (err) {
       result.errorMsg = err.message.split('\n')[0];
   }
   return result;
}


export async function detectDuplicacy(keyReceived, manufacturerTxnId, distributorTxnId) {
    var result = {
        txnHash: '',
        eventDate: '',
        errorMsg: '',
    };

   var duplication_result = 'null';


    try {

	var manufacturerInfo = multichain.getStreamItem({stream: "ipfs_stream",txid:manufacturerTxnId});
	var distribitorInfo = multichain.getStreamItem({stream: "ipfs_stream",txid:distributorTxnId});

	const manufacturerIpfs = manufacturerInfo.data.ipfsHash;
	const distributorIpfs = distribitorInfo.data.ipfsHash;
	if(manufacturerIpfs !== distributorIpfs){
		duplication_result = "Failed";
		console.log("Alert! Duplicate Document");
	} else {
		duplication_result = "Succeeded";
		console.log("Certificate Verification Succeeded");
	}

        result.eventDate = new Date().toLocaleString();
        var dataArgs = {
            stream: 'duplication_stream',
            key: keyReceived,
            data: {
                eventName: 'Duplication_Verification',
	        outcome: duplication_result,
            },
        };
       var res = await axios.post(baseUrl1 + '/publish', dataArgs);
       console.log('Hash:' + res.data.transactionId);        
       result.txnHash = res.data.transactionId;
       result.errorMsg = res.error;
   } catch (err) {
       result.errorMsg = err.message.split('\n')[0];
   }
   return result;
}
