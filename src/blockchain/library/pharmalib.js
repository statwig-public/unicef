import web3 from '../web3';
import MCase from "../mcase";
import DCase from "../dcase";
import ShipCase from "../ShipCase";
import VerifyCase from "../VerifyCase";
import ReceiveCase from "../ReceiveCase";
import ReturnCase from "../ReturnCase";
import Alert from "../alert";

export async function makeManufacturerCase(caseID,srcLocation,bottleList, callback) {
    var result  = {
        txnHash: '',
        eventDate: '',
        errorMsg: ''
    };
    try {
        const accounts = await web3.eth.getAccounts();
        const mCase = MCase();
        result.eventDate = new Date().toLocaleString();
        await mCase.methods.createMCase(caseID,result.eventDate,srcLocation,bottleList).send({
            from: accounts[0],
            gas: '1000000'
        },(err, res) => callback(res));
    } catch (err) {
        result.errorMsg = err.message.split('\n')[0];
    }
    return result;
}

export async function makeDistributorCase(caseID,srcLocation,bottleList, callback) {
    let result  = {
        txnHash: '',
        eventDate: '',
        errorMsg: ''
    };
    try {
        const accounts = await web3.eth.getAccounts();
        const dCase = DCase();
        result.eventDate = new Date().toLocaleString();
        const txHash = await dCase.methods.createDCase(caseID,result.eventDate,srcLocation,bottleList).send({
            from: accounts[0],
            gas: '1000000'
        },(err, res) => callback(res));
        result.txnHash = txHash.transactionHash;
    } catch (err) {
        result.errorMsg = err.message.split('\n')[0];
    }
    return result;
}

export async function shipCase(caseID,shippedBy, shippedTo, destLocation, callback) {
    var result  = {
        txnHash: '',
        eventDate: '',
        errorMsg: ''
    };
    try {
        const accounts = await web3.eth.getAccounts();
        const sCase = ShipCase();
        result.eventDate = new Date().toLocaleString();
        await sCase.methods.shipCase(caseID,destLocation,shippedBy,shippedTo, result.eventDate).send({
            from: accounts[0],
            gas: '1000000'
        },(err, res) => callback(res));
    } catch (err) {
        result.errorMsg = err.message.split('\n')[0];
    }
    return result;
}

export async function receiveCase(caseID,sentBy, receivedBy,callback) {
    var result  = {
        txnHash: '',
        eventDate: '',
        errorMsg: ''
    };
    try {
        const accounts = await web3.eth.getAccounts();
        const recvCase = ReceiveCase();
        result.eventDate = new Date().toLocaleString();
        await recvCase.methods.receiveCase(caseID,sentBy,receivedBy,result.eventDate).send({
            from: accounts[0],
            gas: '1000000'
        },(err, res) => callback(res));
    } catch (err) {
        result.errorMsg = err.message.split('\n')[0];
    }
    return result;
}

export async function verifyCase(caseID,requestBy,requestTo,callback) {
    var result  = {
        txnHash: '',
        eventDate: '',
        errorMsg: ''
    };
    try {
        console.log("Printing on the console");
        const accounts = await web3.eth.getAccounts();
        const verifyCase = VerifyCase();
        result.eventDate = new Date().toLocaleString();
        await verifyCase.methods.verifyCase(caseID,requestBy,requestTo, result.eventDate).send({
            from: accounts[0],
            gas: '1000000'
        },(err, res) => { callback(res)});
    } catch (err) {
        result.errorMsg = err.message.split('\n')[0];
    }
    return result;
}

export async function returnCase(caseID,bottleList,returnBy,returnTo, callback) {
    var result  = {
        txnHash: '',
        eventDate: '',
        errorMsg: ''
    };
    try {
        const accounts = await web3.eth.getAccounts();
        const returnCase = ReturnCase();
        result.eventDate = new Date().toLocaleString();
        await returnCase.methods.returnCase(caseID,bottleList,returnBy,returnTo,result.eventDate).send({
            from: accounts[0],
            gas: '1000000'
        },(err, res) => callback(res));
    } catch (err) {
        result.errorMsg = err.message.split('\n')[0];
    }
    return result;
}

//alertID is created by taking the current date & time stamp and the case ID
export async function recordAlert(caseID,alertType,alertValue,location, callback) {
    var result  = {
        txnHash: '',
        eventDate: '',
        errorMsg: ''
    };
    try {
        const accounts = await web3.eth.getAccounts();
        const alert = Alert();
        result.eventDate = new Date().toLocaleString();
        await alert.methods.createAlert(caseID,alertType,result.eventDate,alertValue,location).send({
            from: accounts[0],
            gas: '1000000'
        },(err, res) => callback(res));
    } catch (err) {
        result.errorMsg = err.message.split('\n')[0];
    }
    return result;
}

export async function fetchBottles(caseID,owner) {
    var result  = {
        bottleList: '',
        errorMsg: ''
    }
    try {
        if (owner === 'manfacturer') {
            //get the latest mcase and check the bottle IDs
            const mcase = MCase();
            const mCaseCount = await mcase.methods.getMCaseCount().call();
            const lastCase = await mcase.methods.mCaseList(mCaseCount-1).call();
            result.bottleList = lastCase.bList;
        } else {
            //when it is requested by pharmacy or hospital
            //get the latest dcase and check the bottle IDS
            const dcase = DCase();
            const dCaseCount = await dcase.methods.getDCaseCount().call();
            const lastCase = await dcase.methods.dCaseList(dCaseCount-1).call();
            result.bottleList = lastCase.bList;

        }
    } catch (err) {
        result.errorMsg = err.message.split('\n')[0];
    }
    return result;
}

