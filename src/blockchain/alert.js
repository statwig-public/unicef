import web3 from './web3';
import Alert from './build/Alert.json';

//0x37d379019d4628BF78b05d9DEE878D85fe72e447

export default () => {
    return new web3.eth.Contract(
        JSON.parse(Alert.interface),'0x5A56c5cB2AF03f701f4BFF14f57Ca4a165f3d888'
    )
};
