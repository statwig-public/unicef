pragma solidity ^0.4.24;

contract MCase {

    //A case made by the manufacturer contains 12 bottles which are shipped
    struct mcase {
       string ID;
       string createDate;
       string srcLocation;
       string bList; //can hold a maximum of 12
    }

    mcase[] public mcaseList;
    mapping(string => bool) mcaseIDs;

    function createMCase (string _ID, string _createDate, string _srcLocation, string _bList) public {

       require(!mcaseIDs[_ID]);

       mcase memory newMCase = mcase({ID: _ID,
                                      createDate: _createDate,
                                      bList: _bList,
                                      srcLocation: _srcLocation
                                      });

       mcaseIDs[_ID] = true;
       mcaseList.push(newMCase);
    }

    function getMCaseCount() public view returns(uint) {
           return mcaseList.length;
    }

}
