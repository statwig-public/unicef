pragma solidity ^0.4.24;

contract ShipCase {

    //A case made by the manufacturer contains 12 bottles which are shipped
    struct medCase {
       string ID;
       string destLocation;
       string shippedBy; //person who ships the cases
       string shippedTo;
       string shippedDate;
    }

    medCase[] public shippedList;
    mapping(string => bool) shippedCaseIDs;

    function shipCase (string _ID, string _destLocation, string _shippedBy, string _shippedTo, string _shippedDate) public {

       require(!shippedCaseIDs[_ID]);

       medCase memory newCase = medCase({ID: _ID,
                                    destLocation: _destLocation,
                                    shippedBy: _shippedBy,
                                    shippedTo: _shippedTo,
                                    shippedDate: _shippedDate
                                    });

       shippedCaseIDs[_ID] = true;
       shippedList.push(newCase);
    }

    function getShippedCaseCount() public view returns(uint) {
       return shippedList.length;
    }
}
