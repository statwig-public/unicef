import web3 from './web3';
import ReturnCase from './build/ReturnCase.json';

//0x37d379019d4628BF78b05d9DEE878D85fe72e447

export default () => {
    return new web3.eth.Contract(
        JSON.parse(ReturnCase.interface),'0x0b57fED64411d77c2F396a7d057040a7Dabb577c'
    )
};
