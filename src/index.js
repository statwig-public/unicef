import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Routes from './frontend/Routes';
import registerServiceWorker from './frontend/registerServiceWorker';
import reducers from "./frontend/reducers";

const store = createStore(
  reducers, /* preloadedState, */
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <Provider store={store}>
    <Routes/>
  </Provider>,
  document.getElementById('root'),
);registerServiceWorker();
