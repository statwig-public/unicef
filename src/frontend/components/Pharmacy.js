import React from 'react';
import BellIcon from 'react-bell-icon';

const pharmacyIcon ="https://statwig.com/pharma/static/media/pharmacy.png";

const Pharmacy = props => {
  return (
    <div className="col-sm-6 pl-5 pr-5 pt-3 pb-2 grey_border custom_receive">
      {props.ringBell.pharmaRingBell && (
        <div className="bell_position">
          <BellIcon
            width="40"
            active={true}
            animate={true}
            color="darkorange"
          />
        </div>
      )}
      <button
        className="btn btn-dark mb-2 btn-block"
        onClick={() => {
          props.showRingBell({ pharmaRingBell: false });
          props.receiveCase('pharma');
          const eventObj = {
            eventName: 'Receive Shipment',
            eventDate: new Date().toLocaleString(),
            owner: 'Manufacturer',
            data: 'location::Hillsdale',
          };
          props.storeEventDetails(eventObj);
          props.history.push('/receiveCasePharma');
        }}
      >
        <i className="fa fa-cart-arrow-down" /> Receive new case
      </button>
      <button
        className="btn btn-dark btn-block position-relative"
        onClick={() => {
          const eventObj = {
            eventName: 'Verified shipment',
            eventDate: new Date().toLocaleString(),
            owner: 'Pharmacy',
            data: 'approvedBy::Distributor',
          };
          props.storeEventDetails(eventObj);
          props.history.push('/verifyCase/pharma');
        }}
      >
        <i className="fa fa-file-text-o">Verification</i>
      </button>
      <img alt="image1" className="image3" src={pharmacyIcon} />
      <h4 className="d-flex align-items-end  justify-content-end">Pharmacy</h4>
    </div>
  );
};

export default Pharmacy;