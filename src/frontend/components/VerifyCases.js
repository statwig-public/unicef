import React from 'react';
const manufacture = "https://statwig.com/pharma/static/media/manufacturer.png";

const VerifyCases = props => {
  return <div className="row justify-content-center mt-5">
    <div className="row ml-0 mr-3 col-sm-5 grey_border">
      <div className="col-sm-6 pl-3 pr-1 pt-3 pb-2">
         <h4>Ethereum</h4>
        <button
          className="btn btn-dark mb-2 btn-block"
          onClick={() => {
            props.history.push('verifyCase/distributor');
          }}
        >
          <i className="fa fa-plus-square" />
          Verify Case
        </button>
      </div>
      <div className="col-sm-3 pl-5 pr-0 pt-3 pb-2">
        <img alt="image1" width="120" height="125" className="image1" src={manufacture} />
        <p className="para1">Distributor</p>
      </div>
    </div>
    <div className="row ml-0 mr-0 col-sm-5 grey_border">
      <div className="col-sm-6 pl-3 pr-1 pt-3 pb-2">
         <h4>Multichain</h4>
        <button
          className="btn btn-dark mb-2 btn-block"
          onClick={() => {
            props.history.push('verifyCaseM/distributor');
          }}
        >
          <i className="fa fa-plus-square" />
         Verify Case
        </button>
      </div>
      <div className="col-sm-3 pl-5 pr-0 pt-3 pb-2">
        <img alt="image1" width="120" height="125" className="image1" src={manufacture} />
        <p className="para1">Distributor</p>
      </div>
    </div>
  </div>
}

export default VerifyCases;