import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Form, Button, Input, Message } from 'semantic-ui-react';
import * as mservices from '../../multichain/pharmalib';

class LocationDuplicacy extends Component {

    state = {
      key:'',
      expectedLatitude:'',
      expectedLongitude:'',
      actualLatitude:'',
      actualLongitude:'',
      status: '',
      distance: '',
    };

    onClick = async () => {

  }

    onSubmit = async (event) => {
 
	var locationData =  mservices.detectLocationDuplicacy(this.state.key);

	var expectedLocationLatitude = locationData.data.expectedLatitude	
	var expectedLocationLongitude = locationData.data.expectedLongitude	

	var actualLocationLatitude = locationData.data.actualLatitude	
	var actualLocationLongitude = locationData.data.actualLongitude	

	this.setState({ expectedLatitude: expectedLocationLatitude });
	this.setState({ expectedLongitude: expectedLocationLongitude });
	this.setState({ actualLatitude: actualLocationLatitude });
	this.setState({ actualLongitude: actualLocationLongitude });

	var Radius = 6371;
	var dLat = (actualLocationLatitude - expectedLocationLatitude)* Math.PI / 180;
	var dLon = (expectedLocationLongitude - actualLocationLongitude)* Math.PI / 180;
	var lat1 = (expectedLocationLatitude)* Math.PI / 180;
	var lat2 = (actualLocationLatitude)* Math.PI / 180;

	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);

	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var dist = Radius * c;
	this.setState({ distance: dist });



	if(this.state.distance <= 2){
		this.setState({ status: 'Valid Product'});
	} else{
		this.setState({ status: 'Duplicate'});
	}


    }; //onSubmit


    render() {
      const thStyle = {
        borderRight: '1px solid grey',
        width: '50%'
      }

      const tdStyle = {
        textOverflow: 'clip'
      }
      
      return (
        <div className="Certificate">
      <div className="w3-row" style={{height: '50px'}}>
        <div className="w3-col s5 w3-xlarge">
          <div className=" w3-left-align logoFont" style={{ paddingLeft: '10px', paddingTop: '5px'}}>
            <span><b>STA</b></span><span style={{color: 'green'}}><b>TWIG</b></span>
          </div>
        </div>
      </div>
          <div style={{height: '90px', backgroundColor: '#0F95D8', textAlign: 'center', color: 'white'}}>
            <h2 style={{marginTop: '10px'}}>Duplicaction Detection with scBlockchain</h2>
            <h4 className="h2Size">Powered by StaTwig</h4>
          </div>

          <div>
            <h3> Enter GTIN for Report </h3>
            <Form onSubmit={this.onSubmit}>


          <Form.Field>
            <Input className="w3-large"
              label="Product GTIN"
              labelPosition="right"
              onChange={event =>
                this.setState({ key: event.target.value })
              }
            />
          </Form.Field>


              <div >
		<div style={{marginLeft: '0px', marginTop: '0px'}}>
                  <button type="submit" className="certificateSumitBtn" >
                    <i className="fa fa-paper-plane-o" aria-hidden="true" style={{ fontSize: '80px' }}></i><br/>
                    GetReport
                  </button >
		</div>
              </div>
              
              
            </Form> 
               
          </div>

<br/>

          <div style={{maxHeight: '450px', backgroundColor: '#0F95D8'}}>
           


            <div className="certificateTableBackground" style={{ borderRadius: '10px',  width: '100%', maxWidth: '90%',  backgroundColor: 'white'}}>
              <div style={{}} className="w3-hide-small">
        <div style={{marginTop: '30px'}} id="bodyContainerLarge" className="w3-container">   
          <Table >
            <tr className="columnHeader">
              <td style={{ borderColor: 'black' }}><b>GTIN</b></td>
              <td style={{ borderColor: 'black' }}><b>Actual Location</b></td>
              <td style={{ borderColor: 'black' }}><b>Expected Location</b></td>
              <td style={{ borderColor: 'black' }}><b>Distance</b></td>
              <td style={{ borderColor: 'black' }}><b>Status</b></td>
            </tr>

            <tr className="columnHeader">
              <td style={{ borderColor: 'black' }}>{this.state.key}</td>
              <td style={{ borderColor: 'black' }}>{"lat: 23.2123; long: 43.1221"}</td>
              <td style={{ borderColor: 'black' }}>{"lat: 24.7172; long: 46.4433"}</td>
              <td style={{ borderColor: 'black' }}>{"2.34 km"}</td>
              <td style={{ borderColor: 'black' }}>{"Valid Document"}</td>
            </tr>

            <tr style={{ borderColor: 'black' }} className="columnHeader">
              <td style={{ borderColor: 'black' }}></td>
              <td style={{ borderColor: 'black' }}></td>
              <td style={{ borderColor: 'black' }}></td>
              <td style={{ borderColor: 'black' }}></td>
              <td style={{ borderColor: 'black' }}></td>
            </tr>

          </Table>

        </div>
              </div>
            </div>
            <br/><br/>
            
          </div>
        </div>
      );
    } 
}


export default LocationDuplicacy;

