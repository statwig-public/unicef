import React from 'react';
import BellIcon from 'react-bell-icon';

const hospitalIcon ="https://statwig.com/pharma/static/media/hospital.png";

const Hospital = props => {
  return <div className="col-sm-6  pl-5 pr-5 pt-3 pb-2 grey_border">
    {props.ringBell.hospitalRingBell && (
      <div className="bell_position">
        <BellIcon
          width="40"
          active={true}
          animate={true}
          color="darkorange"
        />
      </div>
    )}
    <button
      className="btn btn-dark mb-2 btn-block"
      onClick={() => {
        props.showRingBell({ pharmaRingBell: false });
        const eventObj = {
          eventName: 'Received shipment',
          eventDate: new Date().toLocaleString(),
          owner: 'Hospital',
          data: 'approvedBy::Distributor',
        };
        props.storeEventDetails(eventObj);
        props.history.push('/receiveCaseHospital');

      }}
    >
      <i className="fa fa-cart-arrow-down" /> Receive new case
    </button>
    <button
      className="btn btn-dark btn-block position-relative"
      onClick={() => {

        const eventObj = {
          eventName: 'Verified shipment',
          eventDate: new Date().toLocaleString(),
          owner: 'Hospital',
          data: 'approvedBy::Distributor',
        };
        props.storeEventDetails(eventObj);

        props.history.push('/verifyCase/hospital');
      }}
    >
      <i className="fa fa-file-text-o">Verification</i>
    </button>
    <button
      className="btn btn-dark btn-block"
      onClick={() => {
        const eventObj = {
          eventName: 'Return medicines',
          eventDate: new Date().toLocaleString(),
          owner: 'Hospital',
          data: 'sentTo::Distributor,reason::excess',
          caseReturned: true
        };
        props.storeEventDetails(eventObj);
        props.showRingBell({distributorRingBell: true});
        props.history.push('/returnCase');
      }}
    >
      <i className="fa fa-file-text-o">Return</i>
    </button>
    <img alt="image1" className="image4" src={hospitalIcon} />
    <h4 className="d-flex align-items-end  justify-content-end">
      Hospital
    </h4>
  </div>
}

export default Hospital;