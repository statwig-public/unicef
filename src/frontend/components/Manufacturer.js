import React from 'react';
const manufacture = "https://statwig.com/pharma/static/media/manufacturer.png";

const Manufacturer = props => {
  return (
    <div className="row ml-0 mr-0 grey_border">
      <div className="col-sm-6 pl-3 pr-1 pt-3 pb-2">
        {props.multichain ? <h4>Multichain</h4> : <h4>Ethereum</h4>}
        <button
          className="btn btn-dark mb-2 btn-block"
          onClick={() => {
            const eventObj = {
              eventName: 'Aggregation',
              eventDate: new Date().toLocaleString(),
              owner: 'Manufacturer',
              data: 'location::San Jose',
            };
            props.storeEventDetails(eventObj);
            props.history.push(props.newCaseRoute);
          }}
        >
          <i className="fa fa-plus-square" />
          Create a new case
        </button>
        <button
          className="btn btn-dark btn-block"
          onClick={() => {
            const eventObj = {
              eventName: 'Sent shipment',
              eventDate: new Date().toLocaleString(),
              owner: 'Manufacturer',
              data: 'destination:: Santa Clara,batch no:#BTST12345',
            };
            props.storeEventDetails(eventObj);
            props.showRingBell({ distributorRingBell: true });
            props.history.push(props.route);
          }}
        >
          <i className="fa fa-ambulance" />Ship the Case
        </button>
      </div>
      <div className="col-sm-3 pl-5 pr-0 pt-3 pb-2">
        <img alt="image1" width="120" height="125" className="image1" src={manufacture} />
        <p className="para1">Manufacturer</p>
      </div>
    </div>
  );
};

export default Manufacturer;
