import React from 'react';

const TransactionDetails = props => {
  return <div className="transaction_details pt-2 pb-2 col-12 grey_border">
    <h4>Transaction Details</h4>

    <ul className="list-none">
      {props.events.length > 0 && <li>Event name: {props.events[props.txnInd].eventName}</li>}
      {props.events.length > 0 && <li>Event date: {props.events[props.txnInd].eventDate}</li>}
      {props.events.length > 0 && <li>Owner: {props.events[props.txnInd].owner}</li>}
      {props.events.length > 0 && <li>Additional Data: {props.events[props.txnInd].data}</li>}
    </ul>
  </div>
}

export default TransactionDetails;