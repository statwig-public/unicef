import { STORE_CASE_ID } from '../actions';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case STORE_CASE_ID:
      return action.payload;
    default:
      return state;
  }
}
