import { SHOW_RING_BELL } from '../actions';

const INITIAL_STATE = {};

export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SHOW_RING_BELL:
            return action.payload;
        default:
            return state;
    }
}
