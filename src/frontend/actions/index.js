export const STORE_TRANSACTIONS = "STORE_TRANSACTIONS";
export const STORE_MULTICHAIN_TRANSACTIONS = "STORE_MULTICHAIN_TRANSACTIONS";
export const STORE_DISTRIBUTOR_TRANSACTIONS = "STORE_DISTRIBUTOR_TRANSACTIONS";
export const SHOW_RING_BELL = "SHOW_RING_BELL";
export const STORE_EVENT_DETAILS = "STORE_EVENT_DETAILS";
export const STORE_CASE_ID = "STORE_CASE_ID";


export function storeTransaction(transaction) {

  return {
    type: STORE_TRANSACTIONS,
    payload: transaction
  };
}

export function storeMultichainTransaction(transaction) {

  return {
    type: STORE_MULTICHAIN_TRANSACTIONS,
    payload: transaction
  };
}

export function storeDistributorTransaction(transaction) {

  return {
    type: STORE_DISTRIBUTOR_TRANSACTIONS,
    payload: transaction
  };
}
export function storeCaseID(transaction) {

  return {
    type: STORE_CASE_ID,
    payload: transaction
  };
}

export function showRingBell(flag) {

  return {
    type: SHOW_RING_BELL,
    payload: flag
  };
}

export function storeEventDetails(event) {

    return {
        type: STORE_EVENT_DETAILS,
        payload: event
    };
}